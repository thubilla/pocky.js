const Application = require('./src/application');

// Classes
const Router = require('navigo');
const DB = require('lokijs');

const Mediator = require('./src/mediator');
const View = require('./src/view');

// Utilities
const html = require('bel');

const arrayify = require('./src/lib/arrayify');
const cookies = require('./src/lib/cookies');
const getURIParameters = require('./src/lib/get-uri-parameters');
const matrixify = require('./src/lib/matrixify');
const template = require('./src/lib/parse-template');
const randomHex = require('./src/lib/random-hex-value');
const extend = require('./src/lib/this-extend');

exports.Application = Application;

exports.Router = Router;
exports.DB = DB;
exports.Mediator = Mediator;
exports.View = View;

exports.utility = {
	arrayify,
	cookies,
	extend,
	getURIParameters,
	html,
	matrixify,
	template,
	randomHex,
};
