## Pocky.js

*A Thin Front-End Library With a few Core Nutrients, but Mostly (Syntactic) Sugar*

Front-end project boilerplate.

![pocky](http://sweets.seriouseats.com/images/20110328-pocky-unwrapped.jpg)

Application
===

- Publish/Subscribe Methods for Event Handling/Emitting
- In-memory Data Store by [lokijs](http://lokijs.org/#/)
- State/URI Routing by [navigo](https://github.com/krasimir/navigo)

Mediator
===

- Standard Event Bus/Emitter with explicit scope binding

View
===

- Lightweight Modular UI Components built with [bel](https://github.com/shama/bel)
- Update Method for Quick DOM Manipulation/Mutation

Util
===

- [Cookie Storage Interface](https://developer.mozilla.org/en-US/docs/Web/API/Document/cookie/Simple_document.cookie_framework)
- [URI Parameter Interface](http://stackoverflow.com/questions/8648892/convert-url-parameters-to-a-javascript-object)

- Arrayify
    + transform an object into an array
- Matrixify
    + transform an array into a multi-dimensional array matrix
- Template
    + Parse a string of HTML as a Javascript Template Literal
- Random Hex Value
    + generate a random hex value of a specified length
- Extend
    + iterate over an object's parameters and attach each one to a parent

Usage
===

Install as npm Module.

```
npm install --save pocky.js
```

Grab what you need and start using it.

```

const { Observable } = require('pocky.js');
const { html } = require('pocky.js').utility;

// create a new observable
const o = new Observable({ param: false });

// grab some DOM elements
const $container = document.querySelector('#container');
const $btn = $container.querySelector('button');

// define a new DOM element not yet on the screen
const $el = html`<div>
    <h1>The Parameter is Now True!</h1>
</div>`;

// listen for an 'update' event and fire a callback bound to a given context
o.on('update', o, function(update){
    this.param = update;
    $container.appendChild($el);
 });

// fire the 'update' event on user action
$btn.addEventListener('click', function(){
    o.emit('update', true);
});

```

Create a view to render dynamic data.

```

const { View } = require('pocky.js');

// define a reusable view class
class HomePage extends View {
    constructor(options){
        super(options);
    }
}

// define instance of view with a parent node
var $hp = new HomePage({
    parentNode: document.body
 });

// mount view instance to DOM parent
window.addEventListener('load', function(){
    $hp.mount();   
 });

// update the view's data
$hp.data.list = ['red', 'blue', 'yellow'];

// re-render the view instance with the updated data
setTimeout(function(){
    $hp.update();
}, 3000);

```

Build an application.

```

const { Application, View } = require('pocky.js');

// instantiate an app
const app = new Application();

// define the app's routes
app.router.on({
    '/home-page': () => $hp.render(),
    '/update-home-page': () => $hp.update();
});

// create a data collection and insert some data
app.db.addCollection('color-list');
app.db.getCollection('color-list').insert({ color: 'red' });
app.db.getCollection('color-list').insert({ color: 'blue' });
app.db.getCollection('color-list').insert({ color: 'yellow' });

// assign the data collection to something like a view
$hp.data.list = app.db.getCollection('color-list').find();

// navigate to a route
window.addEventListener('load', function(){
    app.router.navigate('/home-page');
});

// do some more stuff later on
setTimeout(function(){
    app.db.getCollection('color-list').insert({ color: 'green' });
    app.db.getCollection('color-list').insert({ color: 'purple' });
    app.db.getCollection('color-list').insert({ color: 'orange' });

    app.router.navigate('/update-home-page');
}, 3000);

```
