/**
	@class
*/
class Mediator {
	/**
		subscribe to event
		@param {event} event - event to subscribe to
		@param {} subscriber - callback context
	*/
	on(event, subscriber){
		if(!this.subscriptions){
			this.subscriptions = {};
		}

		if(!this.subscriptions[event]){
			this.subscriptions[event] = [];
		}

		if(typeof subscriber != 'function'){
			throw new TypeError('Subscriber is not a Function');
		}

		this.subscriptions[event].push(subscriber);
	}
	/**
		check if subscription exists
		@param {event} event - event to subscribe to
		@param {} subscriber - callback context
	*/
	has(event, subscriber){
		if(!this.subscriptions || !this.subscriptions[event]){
			return false;
		}

		return this.subscriptions[event].indexOf(subscriber) >= 0;
	}

	/**
		publish event
		@param {event} event - event to emit
		@param {} context - subscriber
		@param {list} args - pass arguments to callback
	*/
	emit(event, ctx, ...args){
		if(!this.subscriptions || !this.subscriptions[event]){
			return;
		}

		return this.subscriptions[event].forEach(subscription => {
			try{
				subscription.apply(ctx, args);	
			}
			catch(e){
				console.error(e.message);
			}
		});
	}
}

module.exports = Mediator;