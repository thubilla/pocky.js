const test = require('tape');
const EventBus = require('./mediator');

test('Event Bus/Mediator', function(assert){
	assert.comment('.on');

	(function(){
		var bus = new EventBus();
		var subscriber = function(){};
		bus.on('event', subscriber);
		assert.equal(subscriber, bus.subscriptions['event'][0], 'should store a function');
	})();

	(function(){
		var bus = new EventBus();
		var subscribers = [function(){}, function(){}];
		bus.on('event', subscribers[0]);
		bus.on('event', subscribers[1]);

		assert.deepEqual(subscribers, bus.subscriptions['event'], 'should store multiple functions');
	})();

	assert.comment('.has');

	(function(){
		var bus = new EventBus();
		let subscriber = function(){};
		bus.on('event', subscriber);
		assert.ok(bus.has('event', subscriber), 'should return true when has subscription');
	})();

	(function() {
		var bus = new EventBus();
		assert.ok(!bus.has('event', function(){}), 'should return false when no subscription');
	})();

	assert.comment('.emit');

	(function(){
		var bus = new EventBus(),
			subscriber1 = function(){subscriber1.called = true;},
			subscriber2 = function(){subscriber2.called = true;};

		bus.on('event', subscriber1);
		bus.on('event', subscriber2);
		bus.emit('event');

		assert.ok(subscriber1.called, 'should call first subscriber');
		assert.ok(subscriber2.called, 'should call second subscriber');
	})();

	(function(){
		var bus = new EventBus(),
			actual;

		bus.on('event', function(){
			actual = [...arguments];
		});

		bus.emit('event', this, 'String', 1, 32);
		assert.deepEqual(['String', 1 ,32], actual, 'should pass through arguments');
	})();

	(function(){
		var bus = new EventBus();

		assert.doesNotThrow(function(){
			bus.emit('event');
		}, 'should not fail if no subscriptions');
	})();

	(function(){
		var bus = new EventBus(),
			calls = [];

		bus.on('event', function(){
			calls.push('event');
		});

		bus.on('other', function(){
			calls.push('other');
		});

		bus.emit('other');

		assert.deepEqual(['other'], calls, 'should notify relevant subscribers only');
	})();

	assert.comment('Error Handline');

	(function(){
		var bus = new EventBus();

		assert.throws(function(){
			bus.on();
		}, 'TypeError', 'should throw for uncallable subscriber');
	})();

	(function(){
		var bus = new EventBus(),
			subscriber1 = function(){throw new Error('Oops');},
			subscriber2 = function(){subscriber2.called = true;};

		bus.on('event', subscriber1);
		bus.on('event', subscriber2);
		bus.emit('event');

		assert.ok(subscriber2.called, 'should notify all even when some fail');
	})();

	assert.comment('Call Order');

	(function(){
		var bus = new EventBus(),
			calls = [],
			subscriber1 = function(){calls.push(subscriber1);},
			subscriber2 = function(){calls.push(subscriber2);};

		bus.on('event', subscriber1);
		bus.on('event', subscriber2);
		bus.emit('event');

		assert.equal(subscriber1, calls[0], 'should call subscribers in order added');
		assert.equal(subscriber2, calls[1], 'should call subscribers in order added');
	})();	

	assert.end();
});