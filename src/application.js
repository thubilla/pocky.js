const Observable = require('./observable');
const DB = require('lokijs');
const Router = require('navigo');

/**
	@class
*/
class Application extends Observable {
	constructor(options){
		super(options);

		this.db = new DB('db.json');
		this.router = new Router(null, true);

		return this;
	}
}

module.exports = Application;