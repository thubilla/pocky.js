const extend = require('./lib/this-extend');
const Mediator = require('./mediator');
/**
	Portable State Object
	@class
*/
class Observable extends Mediator {
	/**
		@param { object } options - add attributes
	*/
	constructor(options) {
		super();
		/**
			@function
			@param { object } options - add attributes
		*/
		this.extend = extend;
	
		this.extend(options);
		return this;
	}
}

module.exports = Observable;