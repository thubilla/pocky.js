const test = require('tape');
const Observable = require('./observable');

test('Observable Object', function(assert){
	var app = new Observable({ num: 0 });

	assert.plan(8);

	assert.equal(typeof app, 'object', 'is an object');
	assert.equal(typeof app.on, 'function', 'inherits the mediator\'s on method');
	assert.equal(typeof app.has, 'function', 'inherits the mediator\'s has method');
	assert.equal(typeof app.emit, 'function', 'inherits the mediator\'s emit method');

	app.param = true;
	assert.ok(app.param, 'has accessible parameters');

	app.param = false;
	assert.ok(!app.param, 'has configrauble paramaters');

	assert.equal(app.num, 0, 'inits with config parameters');

	app.extend({ extendable: true });
	assert.equal(app.extendable, true, 'remains extendable after init');

	assert.end();
});